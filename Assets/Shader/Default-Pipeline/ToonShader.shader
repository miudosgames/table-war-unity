﻿Shader "Custom/ToonShader"
{
    Properties
    {
		_ShadowStart("Start Shadow", Range(-1,1)) = -1
		_ShadowEnd("End Shadow", Range(-1,1)) = 1

		[HDR] _SpecularColor("Specular Color", Color) = (1,1,1,1)
		_Glossiness("Glossiness", Float) = 32

		_Color("Color", Color) = (1,1,1,1)
		[HDR] _AmbienteColor("Ambiente Color", Color) = (1,1,1,1)
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags 
		{ 
			"RenderType"="Opaque" 
			"LightMode"="ForwardBase"
			"PassFlags"="OnlyDirectinal"
		}
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
			#include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				float3 worldNormal : NORMAL;
				float3 viewDir : TEXCOORD1;
            };

			float _ShadowStart;
			float _ShadowEnd;

			float _Glossiness;
			float4 _SpecularColor;

			fixed4 _Color;
			fixed4 _AmbienteColor;
            sampler2D _MainTex;
            float4 _MainTex_ST;
			
			
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
				o.viewDir = WorldSpaceViewDir(v.vertex);

                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);

				// set Normals
				float3 normal = normalize(i.worldNormal);
				float3 viewDir = normalize(i.viewDir);
				float3 halfVector = normalize(_WorldSpaceLightPos0 + viewDir);

				float NdotL = dot(_WorldSpaceLightPos0, normal);
				float NdotH = dot(normal, halfVector);

				// set Shadows
				//float4 lightIntensity = NdotL > 0 ? 1 : _ShadowIntensity;
				float4 lightIntensity = smoothstep(_ShadowStart, _ShadowEnd, NdotL);
				float4 light = lightIntensity * _LightColor0;

				// set Specular
				float specularIntensity = pow(NdotH * lightIntensity, _Glossiness * _Glossiness);
				float specularIntensitySmooth = smoothstep(0.005, 0.01, specularIntensity);
				float4 specular = specularIntensitySmooth * _SpecularColor;
				

                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);

				return _Color * col * (_AmbienteColor * light * specular);
				//return col;
            }
            ENDCG
        }
    }
}
