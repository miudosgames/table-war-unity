﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
[RequireComponent(typeof(Light))]
public class LightShader : MonoBehaviour
{
    public string nameId = "";

    private Light light_cache = null;
    private Light Light
    {
        get
        {
            if (light_cache == null)
                light_cache = this.GetComponent<Light>();

            return light_cache;
        }
    }


    void Update()
    {
        Shader.SetGlobalVector(string.Format("{0}Direction", nameId), -this.transform.forward);
        Shader.SetGlobalColor(string.Format("{0}Color", nameId), Light.color * Light.intensity);
    }
}
