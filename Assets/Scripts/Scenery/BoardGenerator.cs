﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardGenerator : MonoBehaviour
{
    public HexagonMapData map;
    public List<GameObject> tiles;

    public bool onUpdate;

    public float testValue;
    public static float valueOffset;

    void Start()
    {
        if (!onUpdate)
            UpdateBoard();
    }


    void Update()
    {
        valueOffset = testValue;

        if (onUpdate)
            UpdateBoard();
    }


    void UpdateBoard()
    {
        CleanTiles();
        tiles = CreateHexagonMap(map);
    }


    public void CleanTiles()
    {
        for (int i = tiles.Count - 1; i >= 0; i--)
            Destroy(tiles[i].gameObject);

        tiles.Clear();
    }


    public static List<GameObject> CreateHexagonMap(HexagonMapData map)
    {
        if (map == null)
            return null;

        List<GameObject> tiles = new List<GameObject>();

        for(int i = 0, n = map.layers.Count; i < n; i++)
        {
            HexagonLayer data = map.layers[i];
            List<GameObject> layer = CreateHexagonLayer(data,map.tile);
            tiles.AddRange(layer);

            //for (int a = 0, b = layer.Count; a < b; a++)
            //    layer[a].transform.Translate(0, i * -0.35f, 0);
        }

        return tiles;
    }


    private static List<GameObject> CreateHexagonLayer(HexagonLayer layer, HexagonTileData tileData)
    {
        List<GameObject> tiles = new List<GameObject>();

        for(int i=0,n=layer.tiles.Count; i< n; i++)
        {
            HexagonTile data = layer.tiles[i];
            GameObject tile = Instantiate(tileData.tile);
            SetHexagonTilePosition(tile, data, tileData);
            tiles.Add(tile);

            tile.name += string.Format("[{0}] > x:{1} y:{2}", i, data.x, data.y);
        }

        return tiles;
    }


    private static void SetHexagonTilePosition(GameObject tile, HexagonTile data, HexagonTileData tileData)
    {
        Vector2 position = new Vector2();
        float width = 0;
        float height = 0;

        switch (tileData.orientation)
        {
            case HexagonOrientation.Flat_Topped:
                width = tileData.Corner * (3f / 4f);
                height = tileData.Side;

                Debug.Log("Corner: " + tileData.Corner);
                Debug.Log("Side: " + tileData.Side);

                position = new Vector2()
                {
                    x = data.x * width,
                    y = (data.y * height)
                };

                position.y -= (data.x * height / 2f);

                //if (data.x % 2f != 0)
                //    position.y += height / 2f;

                break;

            case HexagonOrientation.Pointy_Topped:
                {
                    width = tileData.Side;
                    height = tileData.Corner * (3f / 4f);

                    position = new Vector2()
                    {
                        x = data.x * width,
                        y = data.y * height
                    };

                    position.x -= (data.y * width / 2f);

                    //if (data.y % 2f != 0)
                    //    position.x += width / 2f;
                }
                break;
        }


        tile.transform.position = new Vector3(position.x, 0, position.y);
    }

}
