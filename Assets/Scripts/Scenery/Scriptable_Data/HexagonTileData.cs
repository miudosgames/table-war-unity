﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Hexagon_Tile", menuName = "TableWar/Hexagon Tile", order = 0)]
[System.Serializable]
public class HexagonTileData : ScriptableObject
{
    [Header("Mesh")]
    public GameObject tile;

    [Header("Settings")]
    public HexagonOrientation orientation;
    public float radius;


    public float Side { get { return Mathf.Sqrt(3f) * radius; } }
    public float Corner { get { return 2f * radius; } }
}

public enum HexagonOrientation
{
    Flat_Topped,
    Pointy_Topped,
}