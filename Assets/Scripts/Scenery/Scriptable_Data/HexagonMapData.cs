﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Hexagon_Map", menuName = "TableWar/Hexagon Map", order = 1)]
[System.Serializable]
public class HexagonMapData : ScriptableObject
{
    [Header("Tile")]
    public HexagonTileData tile = null;

    [Header("Map")]
    public List<HexagonLayer> layers = new List<HexagonLayer>();
}


[System.Serializable]
public class HexagonLayer
{
    public int id = 0;
    public List<HexagonTile> tiles = new List<HexagonTile>();
}


[System.Serializable]
public class HexagonTile
{
    public int x = 0;
    public int y = 0;
}
